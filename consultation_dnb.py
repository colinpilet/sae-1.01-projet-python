from tkinter import *
from tkinter import ttk
from tkinter.filedialog import *
from tkinter.messagebox import *
import tkinter as tk
import explore_dnb as dnb
import focntion_ajoute as aj

# définition des variable commune

liste_charger = None
session = None
element_choisi = None
liste_fusion = None
cmb = None
entree = None
resultat_convert = None

# Ici vos fonctions dédiées aux interactions

def two_funcs(*funcs):
    def two_funcs(*args, **kwargs):
        for f in funcs:
            f(*args, **kwargs)
    return two_funcs
def validation():
    global cmb, element_choisi
    element_choisi = (cmb.get())

def charger_fichier():
    global liste_charger 
    filename = askopenfilename(title="Charger votre liste", filetypes=[('document csv,','.csv'), ('all files','.*')])
    liste_charger = dnb.charger_resultats(filename)
    showinfo("Fichier chargé", "Vous avez chargé le fichier "+str(filename).split("/")[-1])

def fusion_liste():
    global liste_charger
    charger_fichier()
    liste1 = liste_charger
    charger_fichier()
    liste2 = liste_charger
    liste_charger = dnb.fusionner_resultats(liste1, liste2)
    showinfo("Fichier chargé", "Liste fusionner")

def manipulation():
    global liste_charger
    if liste_charger == None:
        showwarning("Pas de liste", "Veuillez charger une liste")
    else:
        fenetre = Tk()
        fenetre.geometry("400x600")
        taux = LabelFrame(fenetre, text="Taux de réussite", padx=20, pady=10)
        Label(taux, text="Calculer le taux de réussite d'un collège").pack(pady=10)
        bouton(taux, "Choisir une année", choix_session)
        bouton(taux, "Choisir collège", choix_college)
        bouton(taux, "Calculer taux", two_funcs(calculer_taux, fenetre.destroy))
        taux.pack(expand="yes")       
        adm = LabelFrame(fenetre, text="Total admis | présent", padx=20, pady=10)
        Label(adm, text="Calculer le total d'admis et présent pour une année").pack(pady=10)
        bouton(adm, "Choisir une année", choix_session)
        bouton(adm, "Total admis/présent", two_funcs(total_admis_session, fenetre.destroy))
        adm.pack(expand="yes")
        # dep = LabelFrame(fenetre, text="Meilleur collège", padx=20, pady=10)
        # bouton(dep, "Choisir une année", choix_session) 
        # bouton(dep, "Trouver le meilleur collège", meilleur_college)
        # dep.pack(expand="yes")
        quitter(fenetre)
        fenetre.mainloop()

# def meilleur_college():
    # global liste_charger, element_choisi
    # if element_choisi == None:
    #     showwarning("Pas de valeur", "Veuillez choisir une année")
    # else:
    #     print(liste_charger[2])  
    #     best = dnb.meilleur_college(liste_charger, element_choisi)
    #     showinfo("Meilleur collège", "Le meilleur collège pour l'année "+str(element_choisi)+" est "+str(best[0])+" du département "+str(best[1]))       


# def liste_dep():
#     global liste_charger
#     dep = aj.liste_departement(liste_charger)
#     fenetre = Tk()
#     Label(fenetre, text="Sélectionner un département").pack(pady=10)
#     combobox(fenetre, dep)
#     valider_btn(fenetre)
#     quitter(fenetre)
#     fenetre.mainloop()

def choix_session():
    global session
    session = dnb.liste_sessions(liste_charger)
    fenetre = Tk()
    Label(fenetre, text="Sélectionner une année").pack(pady=10)
    combobox(fenetre, session)
    valider_btn(fenetre)
    quitter(fenetre)
    fenetre.mainloop()

def choix_college():
    global element_choisi, liste_charger
    if element_choisi == None:
        showwarning("Pas de valeur", "Veuillez choisir une année")
    else:
        session_filtre = dnb.filtre_session(liste_charger, int(element_choisi))
        nom = aj.nom_college(session_filtre)
        fenetre = Tk()
        Label(fenetre, text="Sélectionner un résultat").pack(pady=10)
        combobox(fenetre, nom)
        valider_btn(fenetre)
        quitter(fenetre)     
        fenetre.mainloop()

def calculer_taux():
    global resultat_convert, element_choisi
    if element_choisi == None or element_choisi.isdigit():
        showwarning("Pas de valeur", "Veuillez choisir un collège")
    else:
        resultat_convert = aj.convert(element_choisi)
        taux = dnb.taux_reussite(resultat_convert)
        showinfo("Taux", "Le taux de réussite de collège "+resultat_convert[1]+" est de "+format(taux,'.2f')+"%")
        element_choisi = None

def meilleur_taux():
    global liste_charger
    if liste_charger == None:
        showwarning("Pas de liste", "Veuillez charger une liste")
    else:
        taux=dnb.meilleur_taux_reussite(liste_charger)
        showinfo("Meilleur taux", "Le meilleur taux de réussite au DNB est de "+format(taux,'.2f')+"%")       

def pire_taux():
    global liste_charger
    if liste_charger == None:
        showwarning("Pas de liste", "Veuillez charger une liste")
    else:
        taux=dnb.pire_taux_reussite(liste_charger)
        showinfo("Pire taux", "Le pire taux de réussite au DNB est de "+format(taux,'.2f')+"%")

def total_admis():
    global liste_charger
    if liste_charger == None:
        showwarning("Pas de liste", "Veuillez charger une liste")
    else:
        total=dnb.total_admis_presents(liste_charger)
        showinfo("Admis | Présent", "Il y a un total de "+str(total[0])+" élèves admis sur "+str(total[1])+" présent depuis "+str(liste_charger[0][0]))

def total_admis_session():
    global liste_charger, element_choisi
    if element_choisi == None:
        showwarning("Pas de liste", "Veuillez choisir une année")
    else:
        total=aj.total_admis_presents_session(liste_charger, int(element_choisi))
        showinfo("Admis | Présent", "Il y a un total de "+str(total[0])+" élèves admis sur "+str(total[1])+" présent pour l'année "+str(element_choisi))
    element_choisi = None

def amelioration():
    global liste_charger
    if liste_charger == None:
        showwarning("Pas de liste", "Veuillez charger une liste")
    else:     
        periode=dnb.plus_longe_periode_amelioration(liste_charger)
        showinfo("Plus longue période", "La plus longue période d'amélioration est entre "+str(periode[0])+" et "+str(periode[1]))


def bouton(fenetre, texte,commande):
    Button(fenetre, text=texte, fg='white', bg='dark blue', height = 1, width = 40, command=commande).pack(pady=2.5)
def quitter(fenetre):
    Button(fenetre, text="Quiter", fg='white', bg='red', height = 1, width = 20, command=fenetre.destroy).pack(side=BOTTOM, pady=15)
def valider_btn(fenetre):
    Button(fenetre, text='Valider', fg='white', bg='dark green', height = 1, width = 20, command=two_funcs(validation, fenetre.destroy)).pack(pady=10)
def combobox(fenetre, valeur):
    global cmb
    cmb = ttk.Combobox(fenetre, width=35,values=valeur, state='readonly')
    cmb.current(0)
    cmb.pack(pady=10)

# ici votre programme principal
def programme_principal():
    global liste_charger
    fenetre = Tk()
    fenetre.geometry("400x400")       
    Label(fenetre, text="Bienvenue, veuillez choisir une action.").pack(pady=15)
    charger = LabelFrame(fenetre, text="Charger une liste", padx=20, pady=10)
    Button(charger, text="Charger documents", fg='White', bg= 'dark green', height = 1, width = 40, command=charger_fichier).pack()
    Button(charger, text="Fusionner liste", fg='White', bg= 'dark green', height = 1, width = 40, command=fusion_liste).pack()
    charger.pack(expand="yes")
    bouton(fenetre, "Trouver la plus longue amélioration", amelioration)
    bouton(fenetre, "Meilleur taux", meilleur_taux)
    bouton(fenetre, "Pire taux de réussite", pire_taux)
    bouton(fenetre, "Nombres admis total", total_admis)
    bouton(fenetre, "Manipulation", manipulation)
    quitter(fenetre)
    fenetre.mainloop()

    

programme_principal()