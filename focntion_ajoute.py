import explore_dnb as dnb


def total_admis_presents_session(liste_resultats, session):
    """calcule le nombre total de candidats admis et de candidats présents aux épreuves du DNB parmi les résultats de la liste passée en paramètre et d'une session

    Args:
        liste_resultats (list): une liste de résultats
        session (int): une session (année)
    Returns:
        tuple : un couple d'entiers contenant le nombre total de candidats admis et présents pour une session
    """
    liste_session=dnb.filtre_session(liste_resultats, session)
    adm =0
    pres = 0
    for i in range(len(liste_session)):
        adm += liste_session[i][4]
        pres += liste_session[i][3]    
    return (adm, pres)

def liste_departement(liste_resultats):
    """retourne la liste des departement dont au moins un résultat est reporté dans la liste de résultats.
    Args:
        liste_resultats (list): une liste de résultats

    Returns:
        list: une liste de departement (int) triée et sans doublons
    """    
    liste =[]
    for i in range(len(liste_resultats)):
        if liste_resultats[i][2] not in liste:
            liste.append(liste_resultats[i][2])
    return liste

def convert(element_choisi):
    """Fonction permmettant de convertir les valeurs récupérées dans une comboboxe en tuple du resultat d'un collège
    """    
    resultat_convert = None
    liste = list(element_choisi)
    lettre = []
    dep = []
    pres = []
    adm = []
    ind = 5
    ind2 = 0
    nom = ""
    if len(liste) == 5:
        resultat_convert=liste
    else:
        session = (int(liste[0])*10**3)+(int(liste[1])*10**2)+(int(liste[2])*10**1)+(int(liste[3]))
        if element_choisi[ind] == "{":
            ind+=1
            while element_choisi[ind] != "}":
                lettre.append(element_choisi[ind])
                ind+=1
            ind+=2
        else:
            while element_choisi[ind] != " ":
                lettre.append(element_choisi[ind])
                ind+=1
            ind+=1
        while ind2 < len(lettre):
            nom+=lettre[ind2]
            ind2+=1
        while element_choisi[ind] != " ":
            dep.append(int(element_choisi[ind]))
            ind+=1
        if len(dep) == 2:
            num=int(dep[0]*10)+(int(dep[1]))
        elif len(dep) == 1:
            num=int(dep[0])
        ind+=1
        while element_choisi[ind] != " ":
            pres.append(int(element_choisi[ind]))
            ind+=1
        if len(pres) == 3:
            nb_pres=int(pres[0]*10**2)+(int(pres[1]*10)+(int(pres[1])))
        elif len(pres) == 2:
            nb_pres=int(pres[0]*10+(int(pres[1])))
        ind+=1
        while ind < len(element_choisi):
            adm.append(int(element_choisi[ind]))
            ind+=1
        if len(adm) == 3:
            nb_adm=int(adm[0]*10**2)+(int(adm[1]*10)+(int(adm[1])))
        elif len(adm) == 2:
            nb_adm=int(adm[0]*10+(int(adm[1])))
        resultat_convert=(session, nom, num, nb_pres, nb_adm)
    return resultat_convert

def nom_college(liste):
    nom = []
    for i in range(len(liste)):
        nom.append(convert(liste[i]))
    return nom