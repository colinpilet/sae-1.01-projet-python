from tkinter import *
from tkinter import ttk
from tkinter.filedialog import *
from tkinter.messagebox import *
import tkinter as tk
import explore_dnb as dnb
# définition des variable commune
liste_charger = None
session = None
element_choisi = None
liste_fusion = None
cmb = None
entree = None
resultat_convert = None
# Ici vos fonctions dédiées aux interactions
def two_funcs(*funcs):
    def two_funcs(*args, **kwargs):
        for f in funcs:
            f(*args, **kwargs)
    return two_funcs
def validation():
    global cmb, element_choisi
    element_choisi = (cmb.get())
def convert():
    """Fonction permmettant de convertir les valeurs récupérées dans une comboboxe en tuple du resultat d'un collège
    """    
    global element_choisi, resultat_convert 
    liste = list(element_choisi)
    lettre = []
    dep = []
    pres = []
    adm = []
    ind = 6
    ind2 = 0
    nom = ""
    session = (int(liste[0])*10**3)+(int(liste[1])*10**2)+(int(liste[2])*10**1)+(int(liste[3]))
    while element_choisi[ind] != "}":
        lettre.append(element_choisi[ind])
        ind+=1
    ind+=2
    while ind2 < len(lettre):
        nom+=lettre[ind2]
        ind2+=1
    while element_choisi[ind] != " ":
        dep.append(int(element_choisi[ind]))
        ind+=1
    if len(dep) == 2:
        num=int(dep[0]*10)+(int(dep[1]))
    elif len(dep) == 1:
        num=int(dep[0])
    ind+=1
    while element_choisi[ind] != " ":
        pres.append(int(element_choisi[ind]))
        ind+=1
    if len(pres) == 3:
        nb_pres=int(pres[0]*10**2)+(int(pres[1]*10)+(int(pres[1])))
    elif len(pres) == 2:
        nb_pres=int(pres[0]*10+(int(pres[1])))
    ind+=1
    while ind < len(element_choisi):
        adm.append(int(element_choisi[ind]))
        ind+=1
    if len(adm) == 3:
        nb_adm=int(adm[0]*10**2)+(int(adm[1]*10)+(int(adm[1])))
    elif len(adm) == 2:
        nb_adm=int(adm[0]*10+(int(adm[1])))
    resultat_convert=(session, nom, num, nb_pres, nb_adm)

def charger_fichier():
    global liste_charger 
    filename = askopenfilename(title="Charger votre liste", filetypes=[('document csv,','.csv'), ('all files','.*')])
    liste_charger = dnb.charger_resultats(filename)

def fusion_liste():
    global liste_charger
    charger_fichier()
    liste1 = liste_charger
    charger_fichier()
    liste2 = liste_charger
    liste_charger = dnb.fusionner_resultats(liste1, liste2)

def liste_session():
    global liste_charger, session
    session = dnb.liste_sessions(liste_charger)

def manipulation():
    global liste_charger
    if liste_charger == None:
        showwarning("Pas de liste", "Veuillez charger une liste")
    else:
        fenetre = Tk()
        fenetre.geometry("400x400")       
        Label(fenetre, text="Manipualtion de la liste").pack(pady=20)
        bouton(fenetre, "Choisir une année", choix_session)
        bouton(fenetre, "Choisir collège", choix_college)
        bouton(fenetre, "calculer taux", calculer_taux)
        quitter(fenetre)

def choix_session():
    global session
    fenetre = Tk()
    Label(fenetre, text="Sélectionner une année").pack(pady=10)
    combobox(fenetre, session)
    valider_btn(fenetre)
    quitter(fenetre)
    fenetre.mainloop()

def choix_college():
    global element_choisi, liste_charger
    if element_choisi == None:
        showwarning("Pas de valeur", "Veuillez choisir une année")
    else:
        session_filtre = dnb.filtre_session(liste_charger, int(element_choisi))
        fenetre = Tk()
        Label(fenetre, text="Sélectionner un résultat").pack(pady=10)
        combobox(fenetre, session_filtre)
        valider_btn(fenetre)
        quitter(fenetre)     
        fenetre.mainloop()

def calculer_taux():
    global resultat_convert, element_choisi
    if element_choisi == None or element_choisi.isdigit():
        showwarning("Pas de valeur", "Veuillez choisir un collège")
    else:
        convert()
        taux = dnb.taux_reussite(resultat_convert)
        showinfo("Taux", "Le taux de réussite de collège "+resultat_convert[1]+" est de "+str(taux)+"%")
        element_choisi = None

def amelioration():
    global liste_charger
    if liste_charger == None:
        showwarning("Pas de liste", "Veuillez charger une liste")
    else:     
        periode=dnb.plus_longe_periode_amelioration(liste_charger)
        showinfo("Plus longue période", "La plus longue période d'amélioration est entre "+str(periode[0])+" et "+str(periode[1]))
def bouton(fenetre, texte,commande):
    Button(fenetre, text=texte, fg='white', bg='dark blue', height = 1, width = 40, command=commande).pack(pady=2.5)
def quitter(fenetre):
    Button(fenetre, text="Quiter", fg='white', bg='red', height = 1, width = 20, command=fenetre.destroy).pack(side=BOTTOM, pady=15)
def valider_btn(fenetre):
    Button(fenetre, text='Valider', fg='white', bg='dark green', height = 1, width = 20, command=two_funcs(validation, fenetre.destroy)).pack(pady=10)
def combobox(fenetre, valeur):
    global cmb
    cmb = ttk.Combobox(fenetre, width=35,values=valeur, state='readonly')
    cmb.current(0)
    cmb.pack(pady=10)

# ici votre programme principal
def programme_principal():
    global liste_charger
    fenetre = Tk()
    fenetre.geometry("400x400")       
    Label(fenetre, text="Bienvenue, veuillez choisir une action.").pack(pady=15)
    Button(fenetre, text="Charger documents", fg='White', bg= 'dark green', height = 1, width = 40, command=charger_fichier).pack()
    Button(fenetre, text="Fusionner liste", fg='White', bg= 'dark green', height = 1, width = 40, command=fusion_liste).pack()
    bouton(fenetre, "Manipulation", two_funcs(manipulation, liste_session))
    bouton(fenetre, "Trouver la plus longue amélioration", amelioration)
    quitter(fenetre)
    fenetre.mainloop()

    

programme_principal()